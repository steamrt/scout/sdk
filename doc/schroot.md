# Building in the Steam Runtime environment using schroot

We recommend using [Docker](docker.md), [Podman](podman.md) or
[Toolbx](toolbx.md) to build games for the Steam Runtime in a
predictable container environment.

However, some developers are more familiar with Debian's schroot tool,
and this can still be used (it is likely to work best on Debian or
Ubuntu machines).
This sets up a [chroot](https://en.wikipedia.org/wiki/Chroot) environment
containing Steam Runtime packages.

The
[steam-runtime Github repository](https://github.com/ValveSoftware/steam-runtime)
contains a script `setup_chroot.sh` which will create a Steam Runtime
chroot on your machine.
You will need the `schroot` tool installed, as well as root access through
`sudo`.
This chroot environment contains the same development libraries and tools
as the Docker container.

You should usually choose a build environment whose version
matches the Steam Runtime bundled with the current general-availability
(non-beta) Steam release.
For convenience,
<https://repo.steampowered.com/steamrt-images-scout/snapshots/latest-steam-client-general-availability/>
always points to that version.

For a different version, such as a beta or a "pinned"
older version, the version-numbered directories in
<https://repo.steampowered.com/steamrt-images-scout/snapshots/>
correspond to the `version.txt` found in official Steam Runtime builds,
typically `~/.steam/root/ubuntu12_32/steam-runtime/version.txt` in a
Steam installation.

For a 64-bit environment, download
`com.valvesoftware.SteamRuntime.Sdk-amd64,i386-scout-sysroot.tar.gz`
from whichever version of the runtime is appropriate, then use a command
like:

    ./setup_chroot.sh --amd64 \
    --tarball ~/Downloads/com.valvesoftware.SteamRuntime.Sdk-amd64,i386-scout-sysroot.tar.gz

This will create a chroot environment named `steamrt_scout_amd64`.

For a 32-bit environment, download
`com.valvesoftware.SteamRuntime.Sdk-i386-scout-sysroot.tar.gz`
from whichever version of the runtime is appropriate, then use a command
like:

    ./setup_chroot.sh --i386 \
    --tarball ~/Downloads/com.valvesoftware.SteamRuntime.Sdk-i386-scout-sysroot.tar.gz

This will create a chroot environment named `steamrt_scout_i386`.

Both roots can co-exist side by side.
The amd64 chroot is intended for compiling 64-bit games and tools: it
contains both 64-bit and 32-bit runtime libraries, together with 64-bit
development files and a partial set of 32-bit development files.
The i386 chroot is intended for compiling 32-bit games and tools, and
only contains 32-bit runtime and development libraries.

Once setup-chroot.sh completes, you can use the **schroot** command to
execute any build operations within the Steam Runtime environment,
for example:

    ~/src/mygame$ schroot --chroot steamrt_scout_i386 -- make -f mygame.mak

The chroot should be set up so that the path containing the build tree is
the same inside as outside the root.

If this path is not within `/home`, it should be added to
`/etc/schroot/default/fstab`.
For example, if you keep source and build trees in `/srv/mygame`, you
could add `/srv` or `/srv/mygame` to `/etc/schroot/default/fstab`.
Then the next time the root is entered, this path will be available inside the root.

The setup script can be re-run to re-create the schroot environment.
For example, you can do this to reset the installed packages to a
"clean slate", or to upgrade to a new `-sysroot.tar.gz` file.

Not using a downloaded tarball
------------------------------

For historical reasons, it is possible to run `setup_chroot.sh` without
using the `--tarball` option.
This will download a minimal Ubuntu 12.04 environment and convert it
into a Steam Runtime environment.
The result is not guaranteed to match the official sysroot tarballs,
and whether it succeeds is heavily dependent on the operating system on
which you are running the tool, so this approach is no longer recommended.
