# scout SDK

This project provides container images for the Steam Runtime 1 'scout' SDK.

For general information about scout, please see the README in the
`steamrt/scout` branch of the `steamrt` package:
<https://gitlab.steamos.cloud/steamrt/steamrt/-/blob/steamrt/scout/README.md>.

## Developing software that runs in scout

For widest compatibility with Linux distributions, native Linux
games and tools shipped via Steam should be compiled in the `scout`
Steam Runtime container environment.
This ensures that all of their dependencies except for glibc and the
graphics drivers are either provided with Steam (in the Steam Runtime),
or included with the game.
If the user has a newer version of one of the library that is included in the
Steam Runtime (which is likely), then the newer version will automatically
be selected at runtime.

We recommend compiling in an OCI-based container using a framework such
as Toolbx, Podman or Docker.
This ensures that only the libraries in the container are used for
compilation, as well as making builds more predictable and reproducible.
See [below](#oci) for more details.

Previous versions of scout, published before container frameworks had
become widespread, recommended using a
[chroot](https://en.wikipedia.org/wiki/Chroot)
environment via the `schroot` tool.
We no longer recommend this, because it is more difficult to set up on
non-Debian-derived machines and requires root privileges, but it is
still possible: see [below](#tar).

Steam will generally run the game with newer libraries available,
either via the [`LD_LIBRARY_PATH` Steam Runtime][ldlp] or via the
[Steam Linux Runtime 1.0 (scout) container runtime][container runtime].
A [guide to the Steam Linux Runtime for game developers][slr-for-game-developers]
is available.

For newer native Linux games, consider targeting
[Steam Runtime 3 'sniper'][sniper SDK], which provides newer shared libraries
in a [container runtime][] environment.
The intention is that this will become available as a "self-service"
feature via the Steamworks partner web interface, which can be used by
any game that benefits from a newer library stack.
However, as of early 2024, this mechanism is not yet ready, so configuring
a game to run in sniper requires manual setup by a Valve developer.
Please contact Valve for more information.

## Testing software that runs in scout

The primary way to test software that runs in scout is to install it
via Steam, and run it via Steam's
[`LD_LIBRARY_PATH`-based runtime][LDLP].

For games that are not yet available via Steam, you can run in the scout
environment using commands like:

```
$ ~/.steam/root/ubuntu12_32/steam-runtime/setup.sh
$ ~/.steam/root/ubuntu12_32/steam-runtime/run.sh ./mygame
```

To check for the ability to run in the *Steam Linux Runtime 1.0 (scout)*
container that will usually be used on Steam Deck, use Steam's
*Properties* → *Compatibility* to select the
*Steam Linux Runtime 1.0 (scout)* tool,
or see the [guide for game developers][slr-for-game-developers].

There is also a [Platform OCI image][] which can be used to run software
that was compiled for scout in a CI environment.
This is particularly useful as a way to check that software compiled for
scout does not have unintentional dependencies on newer libraries.

## OCI images for Docker and Podman <a name="oci"></a>

For Docker ([more information](doc/docker.md)):

    sudo docker pull registry.gitlab.steamos.cloud/steamrt/scout/sdk
    sudo docker pull registry.gitlab.steamos.cloud/steamrt/scout/sdk/i386

or for [Podman](https://podman.io/) ([more information](doc/podman.md)):

    podman pull registry.gitlab.steamos.cloud/steamrt/scout/sdk
    podman pull registry.gitlab.steamos.cloud/steamrt/scout/sdk/i386

or for [Toolbx](https://containertoolbx.org/)
([more information](doc/toolbx.md)):

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/scout/sdk scout
    toolbox enter scout

    toolbox create -i registry.gitlab.steamos.cloud/steamrt/scout/sdk/i386 scout-i386
    toolbox enter scout-i386

or for [Distrobox](https://distrobox.it/)
([more information](doc/distrobox.md)):

    distrobox create -i registry.gitlab.steamos.cloud/steamrt/scout/sdk scout
    distrobox enter scout

    distrobox create -i registry.gitlab.steamos.cloud/steamrt/scout/sdk/i386 scout-i386
    distrobox enter scout-i386

Other OCI-compatible tools should also be compatible with these images.

### Versions

Two repositories are available:

* `registry.gitlab.steamos.cloud/steamrt/scout/sdk` is the 64-bit SDK.
    32-bit versions of some libraries are also available, but most 32-bit
    development libraries are not provided in this SDK. Use this to
    build 64-bit games.

* `registry.gitlab.steamos.cloud/steamrt/scout/sdk/i386` is the purely 32-bit SDK.
    64-bit libraries are not provided. Use this to build legacy 32-bit games.
    Consider building new games and versions as 64-bit instead:
    Steam for Linux already requires a 64-bit CPU, so there is little
    reason to prefer releasing 32-bit binaries.

Tags available for those repositories (can be specified after `:` when
using `docker pull`):

* `0.20241127.109674` (etc.): Specific versions of the SDK. Use one of these
    if you need to "pin" to an older version for reproducible builds.
    These version numbers correspond exactly to
    <https://repo.steampowered.com/steamrt1/images/0.20241127.109674/> (etc.).

* `latest`, `latest-steam-client-general-availability`:
    An alias pointing to the SDK corresponding to the runtime that is included
    in the current General Availability version of the Steam client.
    If in doubt, build games against this.
    This should always match the version in
    <https://repo.steampowered.com/steamrt1/images/latest-steam-client-general-availability/>.

* `beta`, `latest-steam-client-public-beta`:
    An alias pointing to the SDK corresponding to the runtime that is included
    in the current public beta version of the Steam client.
    This should always match the version in
    <https://repo.steampowered.com/steamrt1/images/latest-steam-client-public-beta/>.
    Games built against this runtime will not always work with the
    General Availability version of the Steam client.

These OCI images were prepared using Docker, but should be equally suitable
for other OCI-compatible tools like `podman`.

## Tar archives for schroot and other non-OCI environments <a name="tar"></a>

We recommend Toolbx, Podman or Docker as the preferred way to develop
against scout.
However, for those who are more familiar with the `schroot` tool or other
non-OCI-based chroot and container environments, the `-sysroot.tar.gz`
archives available from
<https://repo.steampowered.com/steamrt1/images/>
contain the same packages as the official OCI images.
[More information about schroot](doc/schroot.md)

If you prefer other chroot or container frameworks such as
`systemd-nspawn`, the same tar archives can be unpacked into a directory
and used with those.

The versions available are the same as for the [OCI images](#oci).

## Toolchains

### C/C++ compilers

The scout SDK contains multiple C/C++ compilers:

* gcc-4.6 and g++-4.6
* gcc-4.8 and g++-4.8 (default)
* gcc-5 and g++-5
* gcc-9 and g++-9
* gcc-12 and g++-12 (experimental)
* clang-3.4 and clang++-3.4
* clang-3.6 and clang++-3.6
* clang-3.8 and clang++-3.8

The experimental `gcc-12` and `g++-12` can be installed by using
`apt-get install gcc-12-monolithic` but are not currently included in
the SDK itself.

For compatibility with older host operating systems, `gcc-9`, `g++-9` and
newer versions always behave as though the `-static-libgcc` and
`static-libstdc++` options had been used.

All C++ code in your project should usually be built with the same compiler.
C-based external dependencies such as SDL and GLib are safe to use, but
passing a STL object such as `std::string` to C++-based libraries from
the Steam Runtime or the operating system (for example `libgnutlsxx` or
`libpcrecpp`) will not necessarily work and is best avoided, especially
if you are using `g++-9` or newer.

Most build systems have a way to use a non-default compiler by specifying
its name, for example Autotools `./configure CC=gcc-5 CXX=g++-5 ...` and
CMake `cmake -DCMAKE_C_COMPILER=gcc-5 -DCMAKE_CXX_COMPILER=g++-5 ...`.

Meson users can use a command like `meson --native-file=gcc-5.txt ...`
for native compilers, or `meson --cross-file=gcc-5-m32.txt ...` to
compile legacy 32-bit games in the 64-bit container.
See `/usr/share/meson/native` and `/usr/share/meson/cross` in the container
for all the options available.

Alternatively, you can change the default by running the
`update-alternatives` command as root inside the container:

    (steamrt scout 0.20220119.0):~$ # for gcc-4.6
    (steamrt scout 0.20220119.0):~$ update-alternatives --auto gcc
    (steamrt scout 0.20220119.0):~$ update-alternatives --auto g++
    (steamrt scout 0.20220119.0):~$ update-alternatives --auto cpp-bin

    (steamrt scout 0.20220119.0):~$ # for gcc-4.8
    (steamrt scout 0.20220119.0):~$ update-alternatives --set gcc /usr/bin/gcc-4.8
    (steamrt scout 0.20220119.0):~$ update-alternatives --set g++ /usr/bin/g++-4.8
    (steamrt scout 0.20220119.0):~$ update-alternatives --set cpp-bin /usr/bin/cpp-4.8

    (steamrt scout 0.20220119.0):~$ # for clang-3.4
    (steamrt scout 0.20220119.0):~$ update-alternatives --set gcc /usr/bin/clang-3.4
    (steamrt scout 0.20220119.0):~$ update-alternatives --set g++ /usr/bin/clang++-3.4
    (steamrt scout 0.20220119.0):~$ update-alternatives --set cpp-bin /usr/bin/cpp-4.8

    (steamrt scout 0.20220119.0):~$ # for clang-3.6
    (steamrt scout 0.20220119.0):~$ update-alternatives --set gcc /usr/bin/clang-3.6
    (steamrt scout 0.20220119.0):~$ update-alternatives --set g++ /usr/bin/clang++-3.6
    (steamrt scout 0.20220119.0):~$ update-alternatives --set cpp-bin /usr/bin/cpp-4.8

### binutils

There are also three implementations of the `binutils` linker, assembler, etc.:

* binutils 2.22 (default)
* binutils 2.30
* binutils 2.35

To use binutils 2.35, prepend `/usr/lib/binutils-2.35/bin` to the `PATH`:

    (steamrt scout 0.20231024.64411):~$ export PATH="/usr/lib/binutils-2.35/bin:$PATH"

or run individual tools using names like `ld-2.35` and `objcopy-2.35`.

`gcc-9`, `g++-9` and newer automatically use binutils 2.35.
Older compilers use binutils 2.22 by default.

### mold linker

An experimental backport of the `mold` linker from Debian 12 is available
for installation via `apt-get install mold` and can be used with
`g++-12 -fuse-ld=mold` or `g++ -B/usr/libexec/mold`.
This is only available for the 64-bit SDK container,
and the `-fuse-ld=mold` command-line option only works with `g++-12`.

## apt packages

scout is based on, and broadly compatible with, Ubuntu 12.04 'precise':
if additional development tools are required, you can install them
from the Ubuntu 12.04 apt repositories. However, please be careful
not to introduce dependencies on packages that are not included in the
runtime.

Additional development tools can also be installed from precise-backports,
from third-party apt repositories compatible with Ubuntu 12.04, or from
source code. Again, please be careful not to introduce dependencies on
packages that are not included in the runtime.

## Source code

Source code for all the packages that go into the OCI image can be found
in the appropriate subdirectory of
<https://repo.steampowered.com/steamrt1/images/>
(look in the `sources/` directory).

The OCI image is built using
[flatdeb-steam](https://gitlab.steamos.cloud/steamrt/flatdeb-steam).
The choice of packages to include in the runtime is partly in the
flatdeb configuration, but mostly controlled by
[the steamrt/scout branch of the steamrt source package](https://gitlab.steamos.cloud/steamrt/steamrt/-/tree/steamrt/scout).

The traditional `LD_LIBRARY_PATH` image is built using
[steam-runtime](https://github.com/ValveSoftware/steam-runtime).
Again, the choice of packages to include in the runtime is partly
in `build-runtime.py`, but mostly controlled by
[the steamrt/scout branch of the steamrt source package](https://gitlab.steamos.cloud/steamrt/steamrt/-/tree/steamrt/scout).

Another relevant project is
[steam-runtime-tools](https://gitlab.steamos.cloud/steamrt/steam-runtime-tools),
which includes the `pressure-vessel` container-runner tool, the
`steam-runtime-system-info` diagnostic tool, and some library code that
they share, and also builds the Steampipe depot used to integrate this
runtime into the SteamPlay compatibility tool mechanism.

[Platform OCI image]: https://gitlab.steamos.cloud/steamrt/scout/platform
[container runtime]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/container-runtime.md
[ldlp]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/ld-library-path-runtime.md
[slr-for-game-developers]: https://gitlab.steamos.cloud/steamrt/steam-runtime-tools/-/blob/main/docs/slr-for-game-developers.md
[sniper SDK]: https://gitlab.steamos.cloud/steamrt/sniper/sdk/-/blob/master/README.md
